package com.example.mycatsproject.data.apiservice.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CatsDto(
    @SerialName("id") val id : String,
    @SerialName("name") val name : String,
    @SerialName("temperament") val temperament : String = "",
    @SerialName("country_code") val countryCode : String = "",
    @SerialName("description") val description : String = "",
    @SerialName("wikipedia_url") val wikipediaUrl : String = "",
)
