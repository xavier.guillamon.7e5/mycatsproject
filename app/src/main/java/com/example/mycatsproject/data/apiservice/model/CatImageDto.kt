package com.example.mycatsproject.data.apiservice.model

import kotlinx.serialization.SerialName

data class CatImageDto(
    @SerialName("url") val imageUrl: String = ""
)
