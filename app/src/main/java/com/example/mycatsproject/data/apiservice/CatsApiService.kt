package com.example.mycatsproject.data.apiservice

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.Retrofit

private const val BASE_URL = "https://api.thecatapi.com/v1/"

private val retrofit = Retrofit.Builder()
    .addConverterFactory(Json.asConverterFactory(MediaType.get("application/json")))
    .baseUrl(BASE_URL)
    .build()

object CatsApi {
    val retrofitService: CatsApiService by lazy {
        retrofit.create(CatsApiService::class.java)
    }
}

interface CatsApiService {
}