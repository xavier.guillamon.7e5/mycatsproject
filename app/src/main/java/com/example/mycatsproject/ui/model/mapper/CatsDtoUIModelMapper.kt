package com.example.mycatsproject.ui.model.mapper

import com.example.mycatsproject.data.apiservice.model.CatImageDto
import com.example.mycatsproject.data.apiservice.model.CatsDto
import com.example.mycatsproject.ui.model.CatsUIModel

class CatsDtoUIModelMapper {
    fun map(breeds: List<CatsDto>, images: List<CatImageDto>) : List<CatsUIModel>{
        return mutableListOf<CatsUIModel>().apply {
            breeds.forEachIndexed { index, catsDto ->
                CatsUIModel(
                    catsDto.id,
                    images[index].imageUrl,
                    catsDto.name,
                    catsDto.temperament,
                    catsDto.countryCode,
                    catsDto.description,
                    catsDto.wikipediaUrl
                )
            }
        }
    }
}