package com.example.mycatsproject.ui.screens


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mycatsproject.data.apiservice.CatsApi
import com.example.mycatsproject.ui.model.CatsUIModel
import com.example.mycatsproject.ui.model.mapper.CatsDtoUIModelMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class CatsViewModel : ViewModel(){
    private val _uiState = MutableStateFlow<List<CatsUIModel>>(emptyList())
    val uiState: StateFlow<List<CatsUIModel>> = _uiState.asStateFlow()

    var mapper = CatsDtoUIModelMapper()
    init {
        viewModelScope.launch {
            val breeds = CatsApi.retrofitService.getCats()
            val photoListDto = breeds.flatMap { breed ->
                CatsApi.retrofitService.getCatImages(breed.id)
                _uiState.value = mapper.map(breeds, photoListDto)
            }
        }
    }
}